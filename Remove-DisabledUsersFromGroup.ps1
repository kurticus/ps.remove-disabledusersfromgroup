<#
.SYNOPSIS
	Removes disabled users from specified Active Directory group
.DESCRIPTION
	This script will remove disabled users from a specific ACtive Direcotry group
.PARAMETER Group
    The name of the group to remove disabled users from
.PARAMETER Credential
    The credentials to use to login to Active Directory
.PARAMETER Server
    The hostname of the Active Directory server where the group exists
.PARAMETER Test
    Setting this switch will enable test mode where only the users that are disabled are listed.
.EXAMPLE
    Remove-DisabledUsersFromGroup -Group "Group Name"
.NOTES
	Author:       Kurt Marvin
	
	Changelog:
	   1.0        Initial release
#>

#region ##################### Parameters #####################
Param 
(
	[Parameter(Mandatory=$true,
               HelpMessage="The name of the group",
               ValueFromPipeline=$true)]
    [ValidateNotNullOrEmpty()]
	[String]$Group,

    [Parameter(Mandatory=$false,
               HelpMessage="The credentials to use to access AD.")]
	[pscredential]$Credential,

    [Parameter(Mandatory=$false,
               HelpMessage="The hostname of the AD server.")]
	[String]$Server,

    [Parameter(Mandatory=$false)]
	[Switch]$Test
)
#endregion

#region ##################### Variables #####################
$groupMembers

#endregion

#region ##################### Functions #####################


#endregion

#region ##################### Main #####################

# Check if the necessary modules exist
if (-not(Get-Module -ListAvailable -Name "ActiveDirectory")) {
    Write-Warning "The necessary module of ActiveDirectory is not installed. Please install the RSAT powershell tools to aquire this module."    
    exit 1
}

if ($Test) { Write-Warning "Running in 'Test' mode. No licenses will be freed." }

if ($Server -and $Credential) {
    $groupMembers = Get-ADGroupMember -Identity $Group -Server $Server -Credential $Credential
} elseif ($Server) {
    $groupMembers = Get-ADGroupMember -Identity $Group -Server $Server
} elseif ($Credential) {
    $groupMembers = Get-ADGroupMember -Identity $Group -Credential $Credential
} else {
    $groupMembers = Get-ADGroupMember -Identity $Group
}

$disabledUsers = @()
$count = 1

foreach ($member in $groupMembers) {
    
    Write-Progress -Activity "Finding Disabled Users: $($disabledUsers.Count)" -Status "Processing User # $count out of $($groupMembers.Count)" -PercentComplete (($count / $groupMembers.Count) * 100)

    if ($Server -and $Credential) {
        $memberDetails = $member | Get-ADUser -Server $Server -Credential $Credential
    } elseif ($Server) {
        $memberDetails = $member | Get-ADUser -Server $Server
    } elseif ($Credential) {
        $memberDetails = $member | Get-ADUser -Credential $Credential
    } else {
        $memberDetails = $member | Get-ADUser
    }
    
    if (-not($memberDetails.Enabled)) {
        $disabledUsers += $memberDetails
    }
    Write-Verbose "Number $count out of $($groupMembers.Count). Total disabled users is $($disabledUsers.Count)"
    $count++
}

if (-not($Test)) {
    if ($Server -and $Credential) {
        Remove-ADGroupMember -Identity $Group -Members $disabledUsers -Server $Server -Credential $Credential
    } elseif ($Server) {
        Remove-ADGroupMember -Identity $Group -Members $disabledUsers -Server $Server
    } elseif ($Credential) {
        Remove-ADGroupMember -Identity $Group -Members $disabledUsers -Credential $Credential
    } else {
        Remove-ADGroupMember -Identity $Group -Members $disabledUsers
    }
}

Write-Progress -Activity "Finding Disabled Users" -Status "Completed" -Completed

Write-Host "Total Disabled Users in Group: $($disabledUsers.Count)"

#endregion